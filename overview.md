# Schmierzettel

# Workflow
- Am Anfang des Tages:
  - Tagesaufgaben aus ToDos / Wichtiges / Als nächstes machen nach Schmierzettel kopieren
- JEDEN TAG MINDESTENS EINE HALBE STUNDE SCHREIBEN
- Jedes neue Wissen / neue Erkenntnis sofort in Schmierzettel dokumentieren
- Am Ende des Tages: 
  - In **Protokolle** dokumentieren, was ich gemacht habe
  - Neue Inhalte in **Aufbau und Zusammenfassung** dokumentieren
  - Fertig bearbeitete Punkte aus **ToDos** löschen
  - Neue **ToDos / Wichtiges** benennen
  - Etwas fertig geschrieben? In **Aufbau und Zusammenfassung** markieren ([x])

# ToDos
## Einführung
- Recherche für Einleitung
  - Allgemeines zu Graphen: Angelika Steger, Diskrete Strukturen?
  - Literatur lesen und zusammenfassen

## Die minimalen NonStick-Graphen
- Definition: Alle induzierten sind Stick
- Äquivalenz: Kein induzierter ist minimaler NonStick
- Graphisch 
  
### Die Eindeutigkeit von Repräsentationen (Grad der Representierbarkeit)
- Symmetrische Representätionen ignorieren
- Nur Rep. mit #horizontal >= #vertikal
- DOKUMENTIEREN: Code und Gedanken
- ungerader Grad: keine symmetrischen, da Partitionen nicht gleich groß
- gerader Grad: Rep. reflektieren, wegschmeißen
- Daraus: Anzahl der Reprädentatioenn (Grad der Representierbarkeit)
- Grad = 1: EINDEUTIGE Repräsentation
### Beweise für Minimale
- Dimension bestimmen. Ist 4 --> NichtStick (Paper)
- Dimension < 4: Ideen?
    - Alle induzierten Subgraphen untersuchen: Größter, der Stick ist und eindeutige Repräsentation hat --> Von dieser ausgehend kann man versuchen, eine Representation zu konstruieren 
    - Wenn kein eindeutiger: niedrigsten Grad nehmen
    - Wenn es zu viele Knoten hinzuzufügen gibt / Es gar keine induz. Subgraphen mit eindeutiger Reduktion / kleinem Grad gibt: Was dann?

## Neue Stick aus alten
- Allgemein: Wann kann man Pfade / Kanten zwische bestehenden Knoten hinzufügen?
- Erweiterungen
  - Definition
  - Zusammenhang zu induzierten Subgraphen 
  - K_4 Erweiterungen: Kreisansatz, innere Pfade > --> Argument mit induzierten Subgraphen

## Statistiken über Graphen
- Wie Daten speichern?
- Länge der Sticks nach Lage (absolut, Mittelwert, Distribution)
- Orientierung der Sticks nach Lage (Distribution, Mittelwert)
- Abstand der Kreuzungen (absolut / relativ)
- Position erster Stick (absolut / relativ)
- Daraus: Heuristik für Ordering im Algo aus Paper
- Algo umsetzen und Heuristik testen
- 
## Schreiben
- Stick und NichtStick bis n = 12 erzeugen
- Finden der minimalen Graphen
- Einleitung

## Sonstiges
### Anhang
- Komprimierung der Repräsentationen dokumentieren (Anhang?)
- Ordnerstruktur von data dokumentieren (Anhang? Implementierungsdeteils?)





# Aufbau & Zusammenfassung: Was habe ich inhaltlich

## Forschungsstand

## Begriffe & Vorbetrachtungen
- Es reicht, zusammenhängende Graphen mit Minimalgrad 2 zu betrachten
- KANONISCHE Stickrepräsentation, Stickvektor
- Spiegelung von Stickrepräsentationen haben den gleichen unterliegenden Graphen. Folgerungen:

## Stick-Graphen kleiner Ordnung
- Alle Stick-Graphen (inklusive ihrer Representationen) bis Ordnung 12 computergestützt gefunden
  - Algorithmus beschreiben
- Daraus: Alle Nicht-Stick-Graphen bis Ordnung 12 gefunden
- Ergebnisse in Zahlen

## Minimale Nicht-Stick-Graphen
- Def: minimaler Nicht-Stick-Graph: Jeder induzierte Teilgraph ist Stick-Graph
- Nicht-Stick-Graph hat keinen minimalen Nicht-Stick-Graph als induzierten Subgraph => Ist minimal
  - Beweis: Zeigen, dass dann alle induzierten Subgraphen stick sind
- Alle bis n=12 erzeugt
  - Bis n=? will ich beweisen, dass diese nicht stick sind
    - Falls Order dimension = 4 --> nicht stick (siehe Arbeit Chaplick)
      - Dimensionen der Minimalen Nicht-Stick-Graphen gefunden (algorithmisch)
    - Falls Order dimension < 4:

# Fertig geschrieben