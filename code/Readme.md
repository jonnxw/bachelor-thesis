# Readme
Der vorliegende Code wurde im Zuge der Bachelorarbeit **Zur Kombinatorik von Stick-Graphen** von Jonathan Wolff erarbeitet. Er ist in Python 2.7.15 mit der Programmbibliothek SageMath 8.1 geschrieben.

## Erläuterungen zur Implementierung in der Arbeit
### Maximal reduzierte Stick-Graphen
Die Erzeugung der Stickvektoren in Abschnitt 6.3.2 ist in der Datei *./Stick.py* implementiert. Folgende Funktionen setzen die beschriebenen Erläuterungen zu Stickvektoren um:

- ``compute_graphs_and_representations``: Die Hauptmethode in dieser Datei erzeugt alle maximal reduzierten Stick-Graphen mit einer gegebenen Anzahl von Knoten. Die Rückgabe ist eine *Map* mit den sparse6-Kodierungen der Stick-Graphen als *Keys*. Der *Value* zu einer sparse6-Kodierung ist eine Liste aller erzeugten Stickvektoren (und damit Stickrepräsentationen) zu diesem Graph. Der Code wird parallel ausgeführt. Durch Änderung das Parameters ``threads`` kann die Anzahl der verwendeten Threads erhöht werden.
- ``all_colorings``: Erzeugt alle zu betrachtenden Färbungen (entspricht den Orientierungen der Sticks) der Ankerpunkte zu einer gegebenen Anzahl Knoten / Sticks.
- ``generate_stickvectors``: Zu einer gegebenen Färbung werden alle Stickvektoren erzeugt, die zu einer kanonischen Stickrepräsentation führen.
- ```generate_stick_from_coloring```: Aus den Stickvektoren werden die zugehörigen Graphen konstruiert.

### Test, ob ein Graph maximal reduziert ist
Dies ist ebenfalls in der Datei *./Stick.py* umgesetzt. Die Methode ```characteristic_int``` berechnet den Wert ```I(v)``` für einen Knoten ```v``` aus der Adjazenzliste von ```v```. Diese Methode wird in ```has_no_congruent_vertices``` genutzt, um zu bestimmen, ob ein Graph keine kongruente Knoten hat. Die Methode ```is_maximal_reduced``` testet zusätzlich, ob ein Graph zusammenhängend ist und den Minimalgrad 2 hat.

### Ableiten der maximal reduzierten bipartiten und Nicht-Stick-Graphen
Sind die maximal reduzierten Stick-Graphen mit *n* Knoten bestimmt, kann die Methode ```DeriveNonStickGraphs``` in der Datei *./Tasks.py* genutzt werden, um daraus die maximal reduzierten bipartiten Graphen sowie die maximal reduzierten Nicht-Stick-Graphen zu bestimmen

### Finden der minimalen Nicht-Stick-Graphen
In Kapitel 7 wird beschrieben, wie aus den erzeugten Daten die minimalen Nicht-Stick-Graphen bestimmt werden können. Die Umsetzung findet sich in der Datei *./MinimalNonStick.py*

## ./Tasks.py
Hier werden Aufgaben gebündelt, wie etwa das Erzeugen und anschließende speichern von Daten. Die drei Hauptaufgaben für diese Arbeit sind:
- ```ComputeStickGraphsAndRepresentations(order)```: Erzeugt die maximal reduzierten Stick-Graphen mit gegebener Anzahl Knoten (```order```). Es wird eine Liste aller gefundenen Graphen gespeichert sowie eine Map, die die Graphen und dazu eine Liste aller zugehörigen Stickvektoren enthält
- ```DeriveNonStickGraphs(order)```: Erzeugen und speichern der maximal reduzierten bipartiten Graphen und der maximal reduzierten Nicht-Stick-Graphen.
- ```FindMinimalNonStick(order)```: Erzeugen und speichern der minimalen Nicht-Stick-Graphen.

## Ausführung des Codes
In der Datei *./script.py* ist Code angegeben, der die maximal reduzierten bipartiten, Stick- und Nicht-Stick-Graphen bis 12 Knoten erzeugt und die minimalen Nicht-Stick-Graphen bis ebenfalls 12 Knoten findet. Er kann in einem Terminal mit dem Befehl ```sage script.py``` ausgeführt werden. In der Datei *./config.py* muss dafür vorher in der Variable ```data_root``` das Wurzelverzeichnis, in dem die erzeugten Daten gespeichert werden sollen, angegeben werden.
