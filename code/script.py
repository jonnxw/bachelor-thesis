# -*- coding: utf-8 -*-
import Tasks

"""
Erzeuge die maximal reduzierten Stick-, bipartiten und Nicht-Stick-Graphen mit maximal 12 Knoten
"""
for n in [6,7,8,9,10,11,12]:
    # Stick-Graphen
    Tasks.ComputeStickGraphsAndRepresentations(n)
    # bipartite und Nicht-Stick-Graphen (diese sind auch bipartit)
    Tasks.DeriveNonStickGraphs(n)
    
    
"""
Findet die minimalen Nicht-Stick-Graphen mit 9 bis 12 Knoten
"""
for n in [9,10,11,12]: 
    #Finde minimale Nicht-Stick-Graphen mit n Knoten
    Tasks.FindMinimalNonStick(n)
