# -*- coding: utf-8 -*-
import sage.all
from sage.graphs.graph_coloring import all_graph_colorings
from sage.graphs.graph import Graph
from sage.categories.cartesian_product import cartesian_product

from multiprocessing import Pool
import time
import sys

from helper import StickRepresentations

def compute_graphs_and_representations(order, threads = 4):
    """ 
    Erzeugt alle maximal reduzierten Stick-Graphen mit der gegebenen Anzahl an Knoten (order). Der Code wird parallel ausgeführt und benutzt die angegebene Anzahl an Threads
    """
    assert order > 5, "There are no connected, min-degree-2 stick graphs without congruent vertices with less than 6 vertices."

    start = time.time()

    colorings = list(all_colorings(order))
    
    p = Pool(threads)
    l = p.map(generate_stick_from_coloring, colorings)
    p.terminate()
    p.join()

    graphs = dict()
    for results in l:
        for tup in results:
            if not tup[0] in graphs:
                graphs[tup[0]] = []
            graphs[tup[0]].append(tup[1])

    end = time.time()
    print("Computed %i maximal reduced stick-graphs with %i vertices in %ds" % (len(graphs), order, end-start,))

    return graphs

def all_colorings(order):
    """
    Erzeugt alle Färbungen der Sticks, bei denen die ersten beiden Sticks horizontal und die letzten beiden Sticks vertikal sind und es mindestens so viele horizontale wie vertikale Sticks gibt. 
    """

    # Initialisiere leeren Graphen
    g = Graph(order-4)

    # Verschiebe Nummeriung um zwei, da die zwei horizontalen Sticks 0 und 1 später eingefügt werden
    labels = {}
    for i in range(order):
        labels[i] = i+2
    g.relabel(labels)

    # Funktion von sage, Generiert alle 2-Knotenfärbungen
    colorings = all_graph_colorings(g, 2)
    for c in colorings:
        # Falls kein horizontaler Stick vorhanden, wird Färbung übersprungen
        if 0 in c.keys(): 
            # Falls kein vertikaler Stick vorhanden, wird eine leere Liste initialisiert
            if not 1 in c.keys(): 
                c[1] = []
            # Mindestens so viele horizontale wie vertikale Sticks
            if len(c[0]) >= len(c[1]):
                # Färbungen der ersten und letzten beiden Sticks
                c[0].insert(0, 1)
                c[0].insert(0,0)
                c[1].append(order-2)
                c[1].append(order-1)
                yield c

def generate_stickvectors(coloring):
    """
    Generator für die Stickvektoren  zu einer gegebenen Färbung.
    """
    l = []

    # Anzahl der Knoten / Sticks aus Färbung bestimmen. coloring[0] enthält die Indizes horizontaler Sticks, coloring[1] die der vertikalen
    r = len(coloring[0]) + len(coloring[1])

    for i in range( r ):
        # Stick i horizontal
        if i in coloring[0]:
            # Mögliche Zielpunkte liegen über vertikalen Sticks, Länge mindestens 2
            l.append([end for end in coloring[1] if end > i + 1])
        
        # Stick i vertikal kriegen Wert -1, um diese als vertikal zu markieren
        else:
            l.append([-1])

    # Kartesisches Produkt erzeugt alle Kombinationen der Zielpunkte der horizontalen Sticks. Vertikale Sticks haben jeweils Wert -1 
    prod = cartesian_product(l)

    # Iteriere über alle Auswahlen eines Zielpunktes für jeden horizontalen Sticks
    for p in prod:
        l = list(p)
        # Iteriere über die Ankerpunkte in umgekehrter Reihenfolge
        for i in reversed(range(r)):
            # Stick i vertikal: Initialisiere Leere Liste
            if l[i] == -1:
                l[i] = []
            
            # Stick i horizontal, Zielpunkt liegt über l[i] --> vertikaler Stick l[i] muss mindestens bis Stick i reichen
            else:
                l[l[i]] = [i]
                # Iteriere über die Länge von Stick i, Mindestlänge 2
                for k in range(i+2, l[i]):
                    # l[k] ist Liste, falls Stick k vertikal ist, dieser hat k als möglichen Zielpunkt
                    if type(l[k]) is list:
                        l[k].insert(0,i)
        
        # Test, ob ein vertikaler Stick keinen möglichen Endpunkt hat. Falls ja, kann diese Auswahl verworfen werden. 
        if len(filter(lambda x: type(x)==list and len(x) == 0, l)) is 0:
            for i in range(len(l)):
                if not type(l[i]) == list:
                    l[i] = [l[i]]
            # Kartesisches Produkt erzeugt Stickvektoren
            for p2 in cartesian_product(l):
                yield p2

def generate_stick_from_coloring(coloring):
    graphs = list()
    for stickvector in generate_stickvectors(coloring):
            # Erzeuge Graph aus Stickvektor
            edgelists = {v: [end for end in range(v+1, stickvector[v] + 1) if end in coloring[1] and stickvector[end] <= v] for v in coloring[0]}
            g = Graph(edgelists)
            sparse6 = g.canonical_label().sparse6_string()

            # Test, ob der Graph maximal reduziert ist
            if is_maximal_reduced(sparse6):
                graphs.append( ( sparse6 , StickRepresentations.toInt(stickvector) ) ) 
    return graphs

def is_maximal_reduced(sparse):
    """
    Testet, ob ein Graph maximal reduziert ist.
    """
    g = Graph(sparse)
    return g.is_connected() and (min(g.degree()) > 1) and has_no_congruent_vertices(sparse)

def has_no_congruent_vertices(sparse):
    g = Graph(sparse)
    return len(set(map(characteristic_int,map(lambda v: g.neighbors(v), g.vertices())))) is len(g)

def characteristic_int(l):                                                             
    s = 0
    for i in l:
        s += 2**i
    return s
