from sage.all import *
def get_dimension(sparse6):
    g = Graph(sparse6)
    c = g.bipartite_color()
    e = g.edges(labels=False)
    for i in range(len(e)):
        if c[e[i][0]] is 1: 
            swap(e,i)
    p = Poset(data=([],e))
    dimension = p.dimension()
    #print( (sparse6, dimension) )
    return dimension

def swap(l,i):
    l[i] = (l[i][1], l[i][0])

