import os, pickle
import config
import GraphUtils, StickRepresentations

def readGraphs(path, order, mapToGraph = False, verbose=False):
    """ Reads a .txt file assuming every line contains a sparse6 string of a graph. """
    data = set()
    filename = absolute_filename(path, order)
    f = open(filename, "r")
    
    for line in f.readlines():
        data.add(line.rstrip("\n"))
    
    f.close()
    
    if verbose: print("Read file %s/%s.txt with %i entries" % (path, str(order), len(data)))
    
    if mapToGraph:
        data = {GraphUtils.simple_graph_from_sparse6(S).copy(immutable=True) for S in data}
        if verbose: print("Mapped data to simple graphs")
    
    return data

def writeGraphs(data, path, order, verbose=False):
    """ Writes a .txt file with sparse6 strings of graph from a given set. If set does not contain strings, its assumed that it are instances of Graph and get converted to sparse strings"""
    mkdir(path, verbose)
    filename = absolute_filename(path, order)
    f = open(filename, "w+")

    # check if first element is a string --> if not, convert all graphs to sparse6 strings 
    if len(data) is 0:
        if verbose: print("Got empty set, writing empty file")
        f.write("")
        f.close()
        return

    else:    
        first = data.pop()
        areSparses = type(first) is str
        data.add(first)

        if not areSparses:
            data = map(lambda G: G.canonical_label().sparse6_string(), data)
            if verbose: print("Mapped data to sparse6 strings")
        

        for l in data:
            f.write("%s\n" % l)
        f.close()

    if verbose: print("Wrote file %s/%s.txt with %i entries" % (path, str(order), len(data)))

def read(path, order):
    f = open(absolute_filename(path, order), "rb")
    data = pickle.load(f)
    f.close()
    return data

def write(data, path, order, verbose=False):
    mkdir(path, verbose)
    f = open(absolute_filename(path, order), "wb+")
    pickle.dump(data, f)
    f.close()

def readRepresentations(order, reduced = False, mapToRepresentations = False, verbose = False):
    data = dict()
    path = "stick/representations"
    if reduced: path = path + "/reduced"
    filename = absolute_filename(path, order)
    f = open(filename, "r")
    
    for line in f.readlines():
        line = line.rstrip("\n")
        key,val = line.split("#")
        val = val.split(",")
        #print("%s : %i" % ( key, len(val)))
        val = map(lambda encoded: int(StickRepresentations.decodeStr(encoded)), val)
        if mapToRepresentations: val = map(lambda i: StickRepresentations.toRepresentation(i), val)
        data[key] = val
    f.close()    
    return data

def writeRepresentations(data, order, reduced = False, verbose = False, mapToInt = False):
    path = "stick/representations"
    if reduced: path = path + "/reduced"
    mkdir(path, verbose)
    filename = absolute_filename(path, order)
    f = open(filename, "w+")

    for key in data.keys():
        if mapToInt:
            data[key] = map(lambda s: StickRepresentations.toInt(s) ,data[key])
        reps = ",".join(map(lambda i: StickRepresentations.encodeInt(i),data[key]))
        f.write(key + "#" + reps + "\n")
    f.close()

def mkdir(path, verbose=False):
    try:
        os.makedirs(absolute_path(path))
        if verbose: print("created directory %s" % path)
    except OSError as exc:
        if verbose: print("directory %s already exists, so it is not created" % path)

# def relative_path(*arg):
#     args = filter(not_empty_string, map(str, list(arg)))
#     return "/".join(args)

def absolute_path(*arg):
    args = filter(not_empty_string, map(str, list(arg)))
    args.insert(0, config.data_root)
    return "/".join(args)

def absolute_filename(*arg):
    args = filter(not_empty_string, map(str, list(arg)))
    args.insert(0, config.data_root)
    return "/".join(args).rstrip(".txt") + ".txt"

def not_empty_string(s):
    return s is not ""
