import math
import OS

def toInt(representation):
    bits = math.ceil(math.log(len(representation),2))
    representation = map(lambda n: fill(bin(n).lstrip("0b"), bits), representation)
    return int("1"+"".join(representation),2)

def toRepresentation(i):
    coded = bin(i)[3:]
    bits = getBitLength(coded)
    numbers = []
    while len(coded) > 0:
        n = coded[:bits]
        coded = coded[bits:]
        numbers.append(int(n,2))
    return tuple(numbers)

def fill(s, i):
    while len(s) < i:
        s = "0" + s
    return s

def getBitLength(s):
    bits = {
        1*1: 1,
        2*1: 1,
        3*2: 2,
        4*2: 2,
        5*3: 3,
        6*3: 3,
        7*3: 3,
        8*3: 3,
        9*4: 4,
        10*4: 4,
        11*4: 4,
        12*4: 4,
        13*4: 4,
        14*4: 4,
        15*4: 4,
        16*4: 4
    }

    return bits[len(s)]

digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
def encodeInt(i):
    coded = ""
    while i > 0:
        coded = coded + digits[i%62]
        i = i//62
    return coded

def decodeStr(s):
    #print(s)
    exp = 0
    result = 0
    for dig in s:
        result = result + digits.index(dig)*(62**exp)
        exp = exp + 1
    return result


def reflect(representation):
    #print(representation)
    l = list(representation)
    l.reverse()
    n = len(l)
    t = tuple(map(lambda i: n-1-i, l))
    #print(t)
    return t

def fromString(s):
    return tuple(map(int, s.split(",")))

def toString(r):
    return ",".join(map(str,r))
