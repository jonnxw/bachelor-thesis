import OS

def uniqueRepresentations(n):
    direction = {True: "Right", False: "Up"}
    f = open(OS.absolute_path("stick/representations/reduced/tikz", n) + ".tex", "w+")
    data = OS.readRepresentations(n, reduced=True, mapToRepresentations=True)
    for sparse in data.keys():
        if len(data[sparse]) is 1:
            #print("HAAAAAAAAAA")
            f.write("\\begin{figure}\n")
            f.write("\\begin{tikzpicture}\n")
            f.write("\t\\begin{gridComplete}{%i}\n" % n)
            rep = data[sparse][0]
            for i in range(len(rep)):
                f.write("\t\t\\stick%s{%i}{%i}\n" % (direction[rep[i] > i], i + 1, rep[i] + 1))
            #print("%s --> %s" % (sparse, data[sparse][0]))
            f.write("\t\\end{gridComplete}\n")
            f.write("\\end{tikzpicture}\n")
            f.write("\\end{figure}\n\n")
            f.write("\\clearpage\n\n")
            
    f.close()
