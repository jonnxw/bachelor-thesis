# Meta / Organisation
- IPE: svg importieren, um Graphen hübscher zeichnen zu können
- Tests mit Python
  - [ ] Welche Umgebungen / Pakete gibt es?
  - [ ] Tests für bestimmte Aufgaben schreiben
- LaTeX
  - [ ] BibTeX
    - [x] Funktionsweise
    - [x] Erste Quellen sammeln (FelsnerOrderDimensio, OrderDimension3)
- Programmierung
  - [ ] Import System
  - [ ] Sage Importe in Python 

# Stick
- [ ] Codeschnipsel / Skripte
  - [ ] Nicht-isolierte erzeugen
  - [ ] zusammenhängende Erzeugen
  - [ ] alle erzeugen
  - [ ] Bipartite, nicht-stick erzeugen
  - [ ] Finden von Subgraphen
  - [ ] Finden von "Pattern" für zusammenhngende machen, dokumentieren
- [ ] Zu jedem Graphen ALLE Repräsentationen erzeugen und speichern
- Order Dimension
  - [ ] Buch aus Mathebib angucken
  - [ ] Paper in meinen Untrlagen durchgehen
  - [ ] StackExchange?

## Nicht-Stick
- [ ] Statistiken: 
- [ ] Idee für n=10,11,12: Aus Würfel eine Kante entfernen und nach diesen Subgraphen suchen (fast-WÜrfel)
- [ ] Bipartite, zusammenhängende, nicht-stick Graphen analysieren: Welche Muster / Strukturen verhindern Repräsentation?
  - [ ] n=8,9 Würfelgraph (S_4): Analyse, warum er keine Stickrepräsentation hat
  - [ ] n=10 "Muster" herausarbeiten: bip., nicht-stick für n=10, die nicht S_4 enthalten
    - grafisch aufbereiten
  - [ ] n=10,11,12: Herausfinden der Order Dimension und Statistiken (Kantenzahl, minimaler / Maximaler Grad, längster Kreis, Farbklassen (wie aufgeteilt), Symmetrie)


# Hook
## Arbeiten raussuchen
- [ ] D.  Catanzaro,   S.  Chaplick,   S.  Felsner,   B.  Halld ́orsson,   M.  Halld ́orsson,T. Hixon, and J. Stacho,Max-point-tolerance  graphs,  Discrete Applied Mathematics,  216(2017), 84–97.
- [ ] B. V. Halld ́orsson, D. Aguiar, R. Tarpine, and S. Istrail,The clark phaseable samplesize problem:  Long-range phasing and loss of heterozygosity in GWAS, Journal of ComputationalBiology, 18 (2011), 323–333.
- [ ] T.  Hixon,Hook  graphs  and  more:   some  contributions  to  geometric  graph  theory,  master’sthesis, TU Berlin, 2013 (Felsner fragen?)
- [ ] M. Soto and C. Thraves,p-box:  A new graph model, Discrete Mathematics and TheoreticalComputer Science, 17 (2015).  18 pages.

# Lesen
- [ ] Felsner, Arbeit über Order Dimension
- [ ] Max Point Tolerance Graphs
- [ ] SIAM Artikel zu NP-Schwere von Order Dimension >= 3