# 17.04.19
 - Arbeit zu Stickgraphen gefunden (https://arxiv.org/abs/1808.10005): Recognition and Drawing of Stick Graphs. Enthält notwendige und hinreichende Bedingungen

# 18.04.19
- Stick Algorithmus: Nicht isolierte für maximal 12 Knoten erzeugt:
  - Erzeugen der Färbungen (mehr oder gleich viele horizontale als vertikale)
  - Zu gegebener Färbung: alle Stick Repräsantationen ohne isolierte Knoten erzeugt

# 23.04
- Alle bipartiten bis 10 Knoten erzeugt und gespeichert. generate_graphs siehe `code/generate.sage`
```Python
for n in [1..10]:
    L = graphs(n, lambda G: G.is_bipartite())
    generate_graphs(n, L, "bipartite")
```

# 24.04
- Alle bipartiten, nicht-stick erzeugt (bis n = 12)
  - n = 8: Nur einer: Würfelgraph (S_4)
  - n = 9: Alle 5 enthalten Würfelgraph
  - n = 10: Von 65 enthalten nur 8 NICHT den Würfelgraph --> analysieren
- Treffen mit Prof. Felsner

# 06.05
- "Pattern" von Subgraphen für Non-Stick bis 11 erzeugt

# 08.05
## Gespräch Felsner
- Statistik Stick_10
- Alle Stick haben Order Dimension 3 
  - wäre gut, nicht-stick zu prüfen, welche Order Dimensio sie haben
  - Arbeit lesen
- Hook erste Implementierung anfangen für erste Ergebnisse


