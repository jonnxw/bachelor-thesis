# 08.05
- [x] JabRef einrichten und Testen, erste Quellen einfügen

# 09.05
- [x] Order Dimension
  - [x] bis n=10 alle Dimensionen berechnet
  - [x] n=11 in chunks der Größe 10 zerlegt und Algorithmus, um chunks einzeln zu bearbeiten
- [x] Order Dimensions Algorhitmus?
  - [x] Drei Quellen rausgesucht (Felsner, Yannakakis, Yanez)
  - [x] Sage-Funktionen benutzt: Erste Ergebnisse (siehe script.py)
- Code Doku
  - [x] bipartite, nicht-stick sowie alle stick Graphen aus Daten generieren (derive_graphs.py)

# 10.05
- Dimensionen von stick, n=11 mithilfe von chunks erzeugt 
  - Eine Datei (57.txt) läuft ewig -> einzelne anschauen, welcher macht Probleme? Anderen Algorithmus verwenden
- TODO: GraphUtils.filterSubGraphs(path, n, list_of_forbidden)
- Code
  - [x] helper.OS: 
    - [x] Object schreiben und laden (Ordner wenn nötig erstellen)
  - [x] helper.SetIO
    - [x] Graphen laden und schreiben
- Order Dimension
    - Code
      - [x] check_dimensions: Pfad mit reingeben
    - [ ] Theorie
      - [ ] Was ist die Order dimension eines Graphen? Quellen?
    - [ ] Dimension eines (bipartiten) Graphen (comparability Graph? Einfach eine Partition als maxima nehmen und eine als Minima? Welche Färbung spielt keine Rolle?)
    - [ ] Macht sage Funktion genau das was sie soll?
- Hook
  - [ ] Arbeiten raussuchen (In Quellenliste nehmen!)
  - [ ] Arbeiten anlesen
  - [ ] Bisherigen Code druchgehen: Was kann ich wiederverwenden?
  - [ ] Ersten Entwurf?
- [ ] Codeschnipsel / Skripte und dokumentieren
  - [ ] Nicht-isolierte erzeugen
  - [ ] zusammenhängende Erzeugen
  - [x] alle erzeugen
  - [x] Bipartite, nicht-stick erzeugen
  - [ ] Finden von Subgraphen

- SCHREIBEN
  - [ ] Zusammenhang
    - [ ] Warum reicht es, zusammenhängende zu betrachten? 
      - [ ] Stick-/Hookrepräsentation von zusammenhangskomponenten können getrennt werden
      - [ ] Order dimension: Zusammenhangskomponenten und Komposition von Posets in Zusammenhang bringen
        - [ ] Quelle suchen: Dimension von "Vereinigung". GigDim?
    - [ ] Formel für die Berechnung aller Graphen aus Zusammenhangskomponenten
  - [ ] Einleitung?
    - [ ] GIG, unsere Spezielfälle
    - [ ] Stick/Hook recognition an open problem?
  - [ ] Algorithmus Stick
    - [ ] Idee über Färbungen
    - [ ] Anmerkungen zur Umsetzung
      - [ ] Symmetrieeigenschaft
      - [ ] verbieten von nicht-isolierten Knoten
      - [ ] ableiten der zusammenhängenden daraus

# Diese WOche
- HABEN ALLE UNIQUE PATTERN MIT 12 KNOTEN EINEN 8-KREIS drin?
- Order dimension
  - [x] n=12 Erzeugen!
  - [ ] order dimension
    - [ ] Als Map!
- SubgraphenPattern:
  - [x] Standard (suksessive) bis n=12: Ordentlichen Code schreiben, neue erzeugen?
  - [ ] Grafische Aufbereitung fertig
  - [ ] Eigenschaften: Order Dimension, Längster Kreis, Hamilton?, minimaler Knotengrad, maximaler Knotengrad, Aufteilung der Farbklassen
  - [ ] Längster Kreis: mit längstem Kreis erzeugen
- Statistiken für alle nicht-stick. NUR ZÄHLEN!?
  - [ ] Kreise der Länge n..n/2?
  - [ ] Aufteilung der Partitionen
  - [ ] maximaler / minimaler Grad
  - [ ] duchschnittlicher Grad
  - [ ] Symmetrie: diameter - radius
  - [ ] Auch für stick machen?
  - [ ] Map/Filter funktionen schreiben, um leicht abfragen zu können?

# Gespräch Felsner 16.05.19; Abschluss Stick
- UNTERTEILUNG von Graph: aufspalten von Kanten: Knoten in Mitte einer Kante hinzufügen
- Graphen in Unique Patterns identifizieren
  - K_4: Welche sind Unterteilung
  - Möbiusleiter und Unterteilung
  - Weitere interessante Graphen?
- Dimension für Unique Patterns bestimmen. Welche haben Ordnung 3
- Bei Unique Patterns mit Ordnung 3: Versuchen zu beweisen / analysieren, warum nicht Stick
- These: Graph ist Unterteilung des K_4 mit Kreislänge >= 6 => kein Stickgraph

