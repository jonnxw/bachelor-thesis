- Am Anfang des Tages / Ende des vorherigen Tages:
  - Welche Aufgaben sind noch offen und will ich heute bearbeiten?
  - Welche neuen Aufgaben will ich heute neu anfangen?
- Regelmäßig (alle 1-2h) aufschreiben (in PROTOCOLS), was ich geschafft habe
- Am Ende des Tages
  - STRUCTURE ergänzen: Neue Ergebnisse einfügen, erweitern
  - TASKS: abgeschlossene Aufgaben abhaken

# PROTOCOLS.md
- jeden Arbeitstag dokumentieren
  - Anfang des Tages: Ziel, welche Aufgaben ich bearbeiten will
  - abhaken, wenn Aufgabe vorerst befriedigend bearbeitet, kurze Zusamenfassung der Ergebnisse

# TASKS.md
- Aufgaben, die anfallen / noch zu erledigen sind
- Dringende Aufgaben mit (D) markieren, wichtige mit (W)
- In Themengebiete / Gruppen einteilen

# STRUCTURE.md
- Glederung erarbeiten
- Zu jedem Punkt stichpunktartig Ergebnisse zusammenfassen, Quellen und Skripte verlinken