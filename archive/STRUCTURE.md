# Abstract / Ziele der Arbeit
- Bestimmen der Anzahl von Stick-, Hook- und BipHookgraphen für kleine Knotenanzahl (bzw. Anzahl Zusammenhängender...)
- Eigenschaften von Graphen, die nicht zu diesen Klassen gehören
# Einführung / Vorbetrachtungen
## Betrachtete Graphenklassen
- Grid Intersection Graphs: Über Kontaktgraphen zu Line intersection Graphs zu Grid intersection Graphs?
- Stickgraphen
- (Bip)Hookgraphen
## Begriffe / Vereinfachungen
- Unlabeled, undirected, simple
- Order Dimension von Graphen
- Zusammenhang
- kanonische Repräsentation
- Repräsentationen sind symmetrisch
## Zählen von Graphen
- Vorüberlegungen: Alle betrachteten Graphenklassen haben die Eigenschaft, dass das Weglassen von isolierten Knoten oder nur Betrachtung von Zusammenhangskomponenten die Zugehörigkeit zur Klasse nicht zerstört
- über Graphen ohne isolierte Knoten: Formel herleiten
- über Zusammenhangskomponenten: Formel herleiten


# Stick
## Implementierung des Erzeugens aller Stickgraphen (bis n=12?)
- Erzeugen der Stickgraphen ohne isolierte Knoten
  1. Alle Färbungen erzeugen
    - erster Knoten ist immer horizontal, letzter Knoten immer vertikal --> Färbungen für n-2 Knoten erzeugen reicht
    - Symmetrie der Repräsentation
      - Spiegeln der Repräsentation ist wieder Stickrepräsentation zu gleichem Graph, horizontale werden dadurch zu vertikalen Knoten
      - --> Färbungen mit mehr vertikalen als horizontalen können ignoriert werden
  2. Endpunkte der horizontalen Knoten festlegen (Länge größer als 0, da keine isolierten Knoten)
  3. Alle möglichen Enden der vertikalen Knoten festlegen
  4. 
## Aus den Daten neue Daten erzeugen
- Die Zusammenhängenden daraus filtern
- Aus nicht-isolierten alle Stickgraphen erzeugen
- Alle bipartiten Graphen, die keine Stickgraphen sind
  
## Untersuchung der bipartiten, nicht-sticken Graphen
### Finden von Subgraphen
- Der Würfelgraph ist der "kleinste" Graph ohne Stickrepräsentation (8 Knoten)
- Alle mit 9 Knoten enthalten Würfel
- 10 Knoten: Nur 8 "neue" Muster. Alle Auflisten
- Bei 11 Knoten: Fast keiner enthält Subgraphen
- 12 Knoten: Alle subgraphen kleinerer Knotenzahl suchen? Welche ohne Würfel, Würfel mit 1-2 Kanten weniger (fast-Würfel)?

- Wie bei n=11 und 12 suchen? Fast-Würfel, Kreise der Länge....
  
### Statistiken für n=10,11,12
- Kantenzahl, minimaler / Maximaler Grad, längster Kreis, Farbklassen (wie aufgeteilt), Symmetrie

### Order Dimension
- Wäre super: Welche Dimensionen haben die nicht-Stick Graphen?
- Für n=8-11: von allen Dimensionen bestimmen
  - Welche haben Grad 3?
- n=12: Welche Betrachten? Nur die mit "neuen" Mustern? Nur die ohne "neue" Muster UND ohne fast-Würfelgraph
# Hook