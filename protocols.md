# Woche 04.11 - 08.11
- Erster Abschnitt von Übersicht geschrieben


# Woche 30.09 - 06.10
## Angefangen
- Latex
  - Algo zum finden der minimalen NonStick

# Woche 30.09 - 06.10
- Anmeldung bei Scheutzow abgeben
- Neue Daten bipartite/stick/non-stick für zusammenhängende, min-degree-2 erzeugt bis n=12
  - Alle Graphen als sparse6
  - Für Stick: alle Representationen gemerkt
  - Statt Representationen als String in Int konvertiert (spart Speicherplatz!) TODO: DOKUMENTIEREN
- Finden minimaler Graphen:
  - Code / Erzeugen
  - Dimensionen bestimmt
- Recherche für Einleitung
  - Literatur raussuchen
    - Arbetiten von Felsner zitiert?
- "Eindeutige Repräsentationen
  - Code fertig, Daten erzeugt
## Angefangen
- Geschrieben
  - Algo zum finden der minimalen NonStick

# Besprechung 27.09
- Graphen mit Grad-1-Knoten wegschmeißen (unnötig, da "Sackgassen" einfach zu Stickrep. hinzugefügt oder daraus entfernt werden können)
  - Daten neu erzeugen (erstmal ohne merken) --> sollten deutlich weniger sein
  - Merken aller Representationen / Anzahl sollte dann bis 12 möglich sein
- unique-patterns / minimale nicht-stick-Graphen
  - für INDUZIERTE Subgraphen machen, für einfache Subgraphen bringt das nichts
  - für gefundene minimale Graphen Beweisen, dass diese nicht-stick sind (Dimensionsargument! Sonst konstruktiv)
- Mathematisch relevante Lemmata suchen
  - Wann kann man Pfad zwischen zwei Knoten hinzufügen? (Siehe Zettel zur Besprechung, konstruktiv)
  - induzierte Subgraphen --> Entfernen von Sticks --> Stickrep von Subgraph enthalten
- Literaturrecherche zu Stick
  - F. De Luca, Recognition and Drawing of Stick Graphs
  - Zusammenfassen, was man über Stickgraphen weiß (Einleitung)
- Erweiterungen des K_4
  - Ansatz mit "äußerem Kreis": Wenn diagonale Pfade mindestens einen Knoten haben, zieht Argument mit induziertem Subgraphen
- Graphen, die nur wenige / eindeutige Stickrep haben
- Order Dimension von nicht-Stick berechnen --> Haben alle Dim. > 3?