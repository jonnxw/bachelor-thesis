#Anzahl der Representationen 
Grundproblem: Ich habe nur die erzeugt, für deren unterliegende Färbung h >= v gilt, um Dopplungen durch Symmetrie möglichst zu vermeiden. Die reale Anzahl der Repr. ist also annähernd doppelt so hoch wie die, ich bestimmt habe. Ausnahme: Graphen mit gerader Ordnung haben Färbungen, bei denen h=v gilt. Dies gilt dann auch für die Spiegelung und somit wird diese Repr. dann wieder doppelt gezählt

Sei $s = (s_1, \dots, s_n)$ ein Stickvektor eines Stickgraphen $G$ der Ordnung $n$. Dann ist $\tilde{s} = (n+1-s_n, \dots, n+1-s_1)$ ebenfalls ein Stickvektor von $G$. Wir nennen $\tilde{s}$ die Spiegelung von $s$ 

# Induzierte Subgraphen
\begin{lemma}
Sei $G = (V,E)$ ein Stick-Graph und $W \subset V$. Dann ist auch $G[W]$ ein Stick-Graph.
\end{lemma}

\begin{lemma}
Sei $G = (V,E)$ ein minimaler Nicht-Stick-Graph und $W \subset V$. Dann ist $G[W]$ ein Stick-Graph.
\end{lemma}
\begin{proof}
Angenommen, $G[W]$ ist ein Nicht-Stick-Graph. 
\end{proof}