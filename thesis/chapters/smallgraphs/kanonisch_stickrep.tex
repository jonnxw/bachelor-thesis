\subsubsection{Kanonische \stickrep{en}}
\label{sec:kanonisch_stickrep}
Die Zeichnung von \stickrep{en} sollen vereinheitlicht werden. Wir betrachten dazu solche, die folgende zwei Eigenschaften haben:

\begin{enumerate}
    \item[(1)] Die An\-ker\-punkte benachbarter Sticks haben alle denselben Abstand.
    \item[(2)] Jeder Stick ist genau bis zu seinem letzten Schnittpunkt mit einem anderen Stick gezeichnet und nicht darüber hinaus.
\end{enumerate}

 Eigenschaft (1) gibt nicht vor, wie groß der Abstand benachbarter Sticks sein soll. Wir vereinheitlichen diesen Abstand und betten dafür eine \stickrep{} $\sr S$ mit $n$ Sticks in ein kartesisches Koordinatensystem (mit nach unten orientierter y-Achse) ein, wobei der An\-ker\-punkt des k-ten Sticks $S_k$ von $\sr S$ im Punkt $\binom k k$ liegt. Die Eigenschaft (2) garantiert, dass für den Zielpunkt $\binom{s_x}{s_y}$ von $S_k$ gilt:
\begin{itemize}
    \item $s_x = k$ und $1 \leq s_y < k$, falls $S_k$ vertikal ist
    \item $s_y = k$ und $k < s_x \leq n$, falls $S_k$ horizontal ist
\end{itemize}
\begin{center}
\begin{tikzpicture}[scale=1]
    \begin{scope}
        \begin{gridPartial}{5}{1,3,5}
            \foreach \lab [count=\i] in {$1$,$\ldots$, $k$, $\ldots$,$n$} {
                \draw [very thin,gray] (\i,1) -- (\i,\i)  node [label=above:$\lab$] at (\i,1) {};
                }
                \foreach \lab [count=\i] in {$1$,$\vdots$, $k$, $\vdots$,$n$} {
                    \draw [very thin,gray] (\i,\i) -- (5,\i) node [label=right:$\lab$] at (5,\i) {};
                    }
                    \stickUpMin[blue, ultra thick] 3 {1.75}
                    \stickRight[dashed, blue] {2.8} {4.9}
                    \stickRight[dashed, blue] 1 {4.95}
                              %\draw[step=1.0, gray] (0,0) grid (6,6); 
                    \draw[->] (0,0) -- (1,0);
                    \draw[->] (0,0) -- (0,1);
                    \node[label=above:$x$] at (0.5,0){};
                    \node[label=left:$y$] at (0,0.5){};
                    \gridCorrectPartial{1,3,5}
        \end{gridPartial}
    \end{scope}
    \begin{scope}[xshift=6cm]
        \begin{gridPartial}{5}{1,3,5}
            \foreach \lab [count=\i] in {$1$,$\ldots$, $k$, $\ldots$,$n$} {
                \draw [very thin,gray] (\i,1) -- (\i,\i)  node [label=above:$\lab$] at (\i,1) {};
                }
                \foreach \lab [count=\i] in {$1$,$\vdots$, $k$, $\vdots$,$n$} {
                    \draw [very thin,gray] (\i,\i) -- (5,\i) node [label=right:$\lab$] at (5,\i) {};
                    }
                    \stickRightMin[blue, ultra thick] 3 {4.25}
                    \stickUp[dashed, blue] {3.2} {1.15}
                    \stickUp[dashed, blue] {5} {1.1}
                    \gridCorrectPartial{1,3,5}
                    %\draw[step=1.0, gray] (0,0) grid (6,6); 
                    % \draw[->] (0,0) -- (1,0);
                    % \draw[->] (0,0) -- (0,1);
                    % \node[label=above:$x$] at (0.5,0){};
                    % \node[label=left:$y$] at (0,0.5){};

        \end{gridPartial}
    \end{scope}
    % \begin{scope}[xshift=12cm]
    %     \begin{gridPartial}{5}{1,3,5}
    %         \foreach \lab [count=\i] in {$1$,$\ldots$, $k$, $\ldots$,$n$} {
    %             \draw [very thin,gray] (\i,1) -- (\i,\i)  node [label=above:$\lab$] at (\i,1) {};
    %             }
    %             \foreach \lab [count=\i] in {$1$,$\vdots$, $k$, $\vdots$,$n$} {
    %                 \draw [very thin,gray] (\i,\i) -- (5,\i) node [label=right:$\lab$] at (5,\i) {};
    %                 }
    %             \node[usual, blue, inner sep=2.5pt, label={[blue,xshift=-2mm,yshift=-1mm]above right:$\binom k k$}] at (3,3){};
    %                 %\draw[step=1.0, gray] (0,0) grid (6,6); 
    %                 % \draw[->] (0,0) -- (1,0);
    %                 % \draw[->] (0,0) -- (0,1);
    %                 % \node[label=above:$x$] at (0.5,0){};
    %                 % \node[label=left:$y$] at (0,0.5){};
    %     \end{gridPartial}
    % \end{scope}
\end{tikzpicture}
\end{center}

Da wir nur maximal reduzierte und damit zusammenhängende Gra\-phen betrachten wollen, lassen wir den Randfall, dass $S_k$ keinen anderen Stick schneidet, außer Acht. Somit sind die beiden genannten Fälle die einzig möglichen. Diese Betrachtungen fassen wir in zwei weiteren Eigenschaften zusammen:

\begin{enumerate}
    \item[(3)] $\sr S$ lässt sich in ein kartesisches Koordinatensystem einbetten, sodass der An\-ker\-punkt des $k$-ten Sticks der Punkt $\binom k k$ ist.
    \item[(4)] Jeder Stick schneidet mindestens einen weiteren Stick.
\end{enumerate}

Eine \stickrep{}, die die Eigenschaften (1) bis (4) erfüllt, nennen wir \textit{kanonisch}. Eigenschaft (2) garantiert auch, dass die Koordinaten der Zielpunkte nur ganzzahlige Werte annehmen. Die möglichen Zielpunktes des $k$-ten Sticks einer kanonischen \stickrep{} lassen sich dann konkret benennen:

\begin{beobachtung}
    Der Zielpunkt des $k$-ten Sticks einer kanonischen \stickrep{} mit $n$ Sticks ist einer der $n-1$ Punkte $\binom{k}{1}$, $\ldots$, $\binom{k}{k-1}$, $\binom{k+1}{k}$, $\ldots$, $\binom{n}{k}$.
    \label{beob:zielpunkt}
\end{beobachtung}

Dies gilt für alle der $n$ Sticks. Mit der Bezeichnung $\mathcal S_n$ für die Menge aller kanonischen \stickrep{en} mit genau $n$ Sticks ergibt sich somit:

\begin{beobachtung}
    Für alle $n \in \mathbb N,~n>1$ gilt: Die Menge $\mathcal S_n$ der kanonischen \stickrep{en} mit genau $n$ Sticks ist endlich und es gilt $|\mathcal S_n| \leq (n-1)^n$.
    \label{beob:kanonisch}
\end{beobachtung}

Es wurde $n>1$ vorausgesetzt, da der Stick einer \stickrep{} mit nur einem Stick keinen weiteren schneiden kann und somit Eigenschaft (4) nicht erfüllt werden kann.\\

Eine \stickrep{} muss nicht kanonisch sein, allerdings ist es möglich, durch Verschieben der An\-ker\-punkte sowie Verlängern und Verkürzen der Sticks aus einer beliebigen \stickrep{} $\sr{S_1}$ eine kanonische \stickrep{} $\sr{S_2}$ mit $G(\sr{S_1}) = G(\sr{S_2})$ zu erzeugen. Folglich hat jeder \stgr{} mit $n$ Knoten auch eine \stickrep{} aus $\mathcal S_n$. Im Umkehrschluss kann man alle \stgren{} mit $n$ Knoten erzeugen, wenn man für alle \stickrep{en} $\sr S\in \mathcal S_n$ den Gra\-phen $G(\sr S)$ bestimmt. 




