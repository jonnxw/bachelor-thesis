In diesem Kapitel werden zwei Familien von \stgren{} behandelt, die eine \textit{eindeutige} \stickrep{} besitzen. Sie werden in späteren Kapiteln benutzt, um für einige Graphen zu beweisen, dass diese Nicht-\stgren{} sind. Zunächst soll jedoch herausgearbeitet werden, was unter einer \textit{eindeutigen \stickrep{}} zu verstehen ist.\\

Es ist möglich, eine \stickrep{} $\sr{S_1}$ an einer Achse, die rechtwinklig zur Grundlinie verläuft, zu spiegeln und so eine \stickrep{} $\sr{S_2}$ zu erhalten. Die horizontalen Sticks von $\sr{S_1}$ werden zu vertikalen Sticks in $\sr{S_2}$ und umgekehrt. Offensichtlich sind $\sr{S_1}$ und $\sr{S_2}$ beide \stickrep{en} desselben Gra\-phen. Erneutes Spiegeln von $\sr{S_2}$ liefert wieder $\sr{S_1}$.

\begin{center}
\begin{tikzpicture}[scale=0.8]
    \begin{scope}
        \begin{gridComplete}{4}
            \stickRight[green] 1 4
            \stickRight[orange] 2 4
            \stickUp 3 2
            \stickUp[blue] 4 1

            \draw[dashed, gray] (2,3) -- (4.5,0.5);
            \node at (2.5,4) {$\sr{S_1}$};
            \gridCorrect{4}
        \end{gridComplete}
    \end{scope}
    \begin{scope}[xshift=5cm]
        \begin{gridComplete}{4}
            \stickRight[blue] 1 4
            \stickRight 2 3
            \stickUp[orange] 3 1
            \stickUp[green] 4 1

            \draw[dashed, gray] (2,3) -- (4.5,0.5);
            \node at (2.5,4) {$\sr{S_2}$};
            \gridCorrect{4}
        \end{gridComplete}
    \end{scope}
    \begin{scope}[xshift=10cm]
        \begin{gridComplete}{4}
            \stickRight[green] 1 4
            \stickRight[orange] 2 4
            \stickUp 3 2
            \stickUp[blue] 4 1

            \draw[dashed, gray] (2,3) -- (4.5,0.5);
            \node at (2.5,4) {$\sr{S_1}$};
            \gridCorrect{4}
        \end{gridComplete}
    \end{scope}

    \draw[->] (5,-2.5) -- (6,-2.5);
    \draw[->] (10,-2.5) -- (11,-2.5) ;
\end{tikzpicture}
\end{center}

Möchte man eine \stickrep{} eines bipartiten Gra\-phen $G=(A\cup B)$ zeichnen, ist man frei in der Wahl, ob die Knoten aus $A$ zu horizontalen oder vertikalen Sticks korrespondieren, da man durch Spiegelung eine entsprechende \stickrep{} mit vertauschten Rollen erhält.

Sind die Par\-ti\-tions\-klassen von $G$ unterschiedlich groß, so sind $\sr{S_1}$ und $\sr{S_2}$ nicht identisch, da die Anzahl der horizontalen Sticks in $\sr{S_1}$ und $\sr{S_2}$ nicht identisch ist. Auch in dem Fall, dass die Partitionsklassen gleich groß sind, führt die Spiegelung einer \stickrep{} im Allgemeinen nicht zu der gleichen \stickrep{}. Sind $\sr{S_1}$ und $\sr{S_2}$ jedoch die einzigen \stickrep{en} von $G$, bezeichnen wir diese trotzdem als eindeutig. Ein Beispiel einen Gra\-phen, der eine eindeutige \stickrep{} in diesem Sinne hat, ist der vollständige bipartite Graph $K_{m,n}$. Die beiden Par\-ti\-tions\-klassen bestehen aus $m$ und $n$ Knoten und jeder Knoten ist mit jedem Knoten der anderen Par\-ti\-tions\-klasse verbunden. In einer \stickrep{} muss also jeder horizontale Stick mit jedem vertikalen Stick verbunden sein. Folglich müssen alle horizontalen Sticks über den vertikalen liegen. Für $m \neq n$ führt das zu zwei möglichen \stickrep{en}. Für beispielsweise $m=2$ und $n=3$ sehen diese wie folgt aus:

\begin{center}
\begin{tikzpicture}[scale=0.8]
    \begin{scope}[scale=0.75, yshift=-4cm]
        \node[usual, green] (a1) at (0,0){};
        \node[usual, green] (a2) at (2,0){};
        \node[usual, green] (a3) at (4,0){};
        \node[usual, orange] (b1) at (1,2){};
        \node[usual, orange] (b2) at (3,2){};
        \draw
        (a1) -- (b1)
        (a1) -- (b2)
        (a2) -- (b1)
        (a2) -- (b2)
        (a3) -- (b1)
        (a3) -- (b2);
    \end{scope}
    \begin{scope}[xshift=4cm, scale=0.75]
        \begin{gridComplete}{5}
            \stickRight[green] 1 5
            \stickRight[green] 2 5
            \stickRight[green] 3 5
            \stickUp[orange] 4 1
            \stickUp[orange] 5 1
            \gridCorrect{5}
        \end{gridComplete}
    \end{scope}
    \begin{scope}
        \begin{scope}[xshift=9cm, scale=0.75]
            \begin{gridComplete}{5}
                \stickRight[orange] 1 5
                \stickRight[orange] 2 5
                \stickUp[green] 3 1
                \stickUp[green] 4 1
                \stickUp[green] 5 1
                \gridCorrect{5}
            \end{gridComplete}
        \end{scope}
    \end{scope}
\end{tikzpicture}
\end{center}

Da diese beiden \stickrep{en} Spiegelungen voneinander und die einzigen \stickrep{en} des $K_{m,n}$ sind, sind diese eindeutig in dem beschriebenen Sinne.