\section{Isomorphismen zwischen Graphen}
\label{sec:iso_auto}
Bei dem Gra\-phen und den \stickrep{en} in \cref{fig:beispiel_gig_stick} sind die Knoten und Sticks nicht beschriftet. Die Existenz und Gestalt einer \stickrep{} zu einem Gra\-phen ist unabhängig von der Bezeichnung der Knoten. Lediglich die Adjazenzen zwischen den Knoten, also die Struktur des Gra\-phen, ist relevant. Dies führt zu dem Begriff der \textit{Isomorphie} von Gra\-phen:
\begin{definition*}
    Zwei Gra\-phen $G$ und $H$ heißen \textit{isomorph}, falls es eine bijektive Abbildung $f: V(G) \to V(H)$ gibt, für die gilt: Zwei Knoten $v, w \in V(G)$ sind genau dann adjazent (in $G$), wenn $f(v)$ und $f(w)$ adjazent (in $H$) sind. Die Abbildung $f$ heißt \textit{Isomorphismus} zwischen $G$ und $H$. 
    \label{def:isomorph}
\end{definition*}

\begin{figure}[!htb]
    \centering
    \begin{tikzpicture}[scale=0.8]
        \begin{scope}
            \node[usual, label=above:$1$] (v1) at (0,2){};
            \node[usual, label=above:$2$] (v2) at (2,2){};
            \node[usual, label=below:$3$] (v3) at (0,0){};
            \node[usual, label=below:$4$] (v4) at (2,0){};

            \node at (-1,1) {$G$};

            \draw (v1) -- (v2) -- (v4) -- (v3) -- (v1);
        \end{scope}
        \begin{scope}[xshift = 4cm]
            \node[usual, label=above:$a$] (v1) at (0,2){};
            \node[usual, label=below:$d$] (v2) at (2,0){};
            \node[usual, label=below:$b$] (v3) at (0,0){};
            \node[usual, label=above:$c$] (v4) at (2,2){};

            \node at (3,1) {$H$};
            \draw (v1) -- (v2) -- (v4) -- (v3) -- (v1);
        \end{scope}
    \end{tikzpicture}
    \caption{Zwei isomorphe Graphen $G$ und $H$}
    \label{fig:isomorphe_bsp}
\end{figure}

Die beiden Graphen $G$ und $H$ in \cref{fig:isomorphe_bsp} sind isomorph. Ein Isomorphismus $f: V(G) \to V(H)$ ist durch $f(1) = a$, $f(2) = d$, $f(3) = b$ und $f(4) = c$ gegeben. Ein Isomorphismus zwischen zwei Graphen ist im Wesentlichen eine Umbenennung der Knoten. Da \stickrep{en} unabhängig von der Benennung der Knoten sind, haben isomorphe Graphen dieselben \stickrep{en}. Jede \stickrep{} eines Graphen $G$ ist auch eine \stickrep{} jedes zu $G$ isomorphen Graphen. 

Die Frage, ob zwei Graphen isomorph zueinander sind, wird auch \textit{Gra\-phen-Iso\-mor\-phie-Problem} genannt. Es ist noch nicht bekannt, ob es in polynomieller Zeit lösbar ist. Das Gra\-phen-Isomorphie-Problem ist äquivalent zu dem Problem, für einen beliebigen Gra\-phen eine \textit{kanonische Form} zu finden. Diese ist eine kanonische Bezeichnung der Knoten, sodass zwei Gra\-phen genau dann isomorph sind, wenn sie dieselbe kanonische Form besitzen. Die kanonische Form wird auch \textit{kanonisches Labeling} genannt. In \cite{mckay1981practical} wird ein Algorithmus vorgestellt, der ein kanonisches Labeling eines Gra\-phen findet und in der Praxis sehr effizient arbeitet.

In dieser Arbeit wird nicht zwischen isomorphen Graphen unterschieden. Eine Formulierung wie \enquote{\textit{Sei $G$ ein Graph}} bedeutet, dass die folgenden Ausführungen auch für jeden zu $G$ isomorphen Graphen gelten. Insbesondere in Beweisen hat das zur Folge, dass zur besseren Verständlichkeit eine Bezeichnung der Knoten frei gewählt werden kann. Wenn Mengen, Familien oder Klassen von Graphen benannt oder bestimmt werden, bezieht sich beispielsweise die Kardinalität der Menge auf die Anzahl der nicht-isomorphen Graphen.\\

Es werden \stickrep{en} gezeichnet, in der einzelnen Sticks bestimmte Knoten zugeordnet werden. Diese Zuordnung ist im Allgemeinen nicht eindeutig. Der Graph $G$ in \cref{fig:isomorphe_bsp} ist der Kreisgraph $C_4$. Für eine \stickrep{} von $G$, die sich später als die eindeutige \stickrep{} des Kreisgraphen herausstellen wird, gibt es acht solcher Zuordnungen, die in \cref{fig:kreis_auto} abgebildet sind.\\

\begin{figure}
\centering
    \begin{tikzpicture}[scale=0.75]
        \begin{scope}[xshift=0cm, yshift=0cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{1}
                \anchorLabelNode[-0.1cm]{2}{4}
                \anchorLabelNode[-0.1cm]{3}{2}
                \anchorLabelNode[-0.1cm]{4}{3}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=5cm, yshift=0cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{1}
                \anchorLabelNode[-0.1cm]{2}{4}
                \anchorLabelNode[-0.1cm]{3}{3}
                \anchorLabelNode[-0.1cm]{4}{2}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=10cm, yshift=0cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{2}
                \anchorLabelNode[-0.1cm]{2}{3}
                \anchorLabelNode[-0.1cm]{3}{1}
                \anchorLabelNode[-0.1cm]{4}{4}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=15cm, yshift=0cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{2}
                \anchorLabelNode[-0.1cm]{2}{3}
                \anchorLabelNode[-0.1cm]{3}{4}
                \anchorLabelNode[-0.1cm]{4}{1}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=0cm, yshift=-5cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{3}
                \anchorLabelNode[-0.1cm]{2}{2}
                \anchorLabelNode[-0.1cm]{3}{1}
                \anchorLabelNode[-0.1cm]{4}{4}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=5cm, yshift=-5cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{3}
                \anchorLabelNode[-0.1cm]{2}{2}
                \anchorLabelNode[-0.1cm]{3}{4}
                \anchorLabelNode[-0.1cm]{4}{1}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=10cm, yshift=-5cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{4}
                \anchorLabelNode[-0.1cm]{2}{1}
                \anchorLabelNode[-0.1cm]{3}{2}
                \anchorLabelNode[-0.1cm]{4}{3}
            \end{gridComplete}
        \end{scope}
        \begin{scope}[xshift=15cm, yshift=-5cm]
            \begin{gridComplete}{4}
                \stickRight 1 4
                \stickRight 2 4
                \stickUp 3 1
                \stickUp 4 1

                \anchorLabelNode[-0.1cm]{1}{4}
                \anchorLabelNode[-0.1cm]{2}{1}
                \anchorLabelNode[-0.1cm]{3}{3}
                \anchorLabelNode[-0.1cm]{4}{2}
            \end{gridComplete}
        \end{scope}
    \end{tikzpicture}
\caption{Acht Zuordnungen von Knoten zu Sticks in einer \stickrep{} des Kreisgraphen $C_4$}
\label{fig:kreis_auto}
\end{figure}

Dies führt zum Begriff des \textit{Automorphismus}. Dies ist ein Isomorphismus eines Graphen auf sich selbst. Aus der Abbildung wird ersichtlich, dass eine Umbenennung der Knoten in der \stickrep{} die Struktur des Graphen nicht verändert. Ein Automorphismus definiert also eine Art von \textit{Symmetrie} des Graphen. Die Menge der Automorphismen eines Graphen $G$ auf sich selbst wird mit $Aut(G)$ bezeichnet. Für einen Kreisgraphen $C_n$ mit $n$ Knoten gilt stets $|Aut(C_n)| = 2n$. Diese entstehen, anschaulich gesprochen, durch \textit{Rotation} und \textit{Spiegelung} der Zeichnung des Kreises.\\

Für das Zeichnen einer \stickrep{} ist die Zuordnung von Sticks zu Knoten unerheblich. Alle \stickrep{} in \cref{fig:kreis_auto} haben dieselbe Struktur und werden als dieselbe \stickrep{} angesehen. In Beweisen wird es sich als nützlich erweisen, eine Zuordnung einzelner Knoten zu Sticks vorzunehmen: In einigen Fällen ist diese Zuordnung auf Grund der Struktur des Graphen doch eindeutig und in anderen Fällen führt eine Zuordnung zu Widersprüchen. Beide Fälle können dazu genutzt werden, die Eindeutigkeit oder Nicht-Existenz einer \stickrep{} zu zeigen.\\

Betrachtet man nochmals die acht Zuordnungen der Knoten in der \stickrep{} des Kreisgraphen $C_4$, so fällt ein Detail auf: Zu jedem Knoten $v$ und jedem Stick $S$ gibt es (mindestens) eine Zuordnung der Knoten zu Sticks, in der $v$ zu $S$ zugeordnet ist. Der Grund liegt darin, dass Kreisgraphen \textit{knotentransitiv} sind. Ein Graph heißt knotentransitiv, falls es zu je zwei Knoten $u,v$ einen Automorphismus $f$ gibt, sodass $f(u)=v$ gilt. Beim Konstruieren von \stickrep{en} hat diese Eigenschaft zur Folge, dass ohne Beschränkung der Allgemeinheit ein Knoten des Graphen einem Stick zugeordnet werden kann. Durch einen entsprechenden Automorphismus kann dann die Zuordnung der Knoten so geändert werden, dass jeder beliebige Knoten zu diesem Stick korrespondiert. 
