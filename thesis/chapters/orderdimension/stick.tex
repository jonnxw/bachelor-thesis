\FloatBarrier 
\subsection{\entwurf{}Die Order Dimension von \stgren}
Da alle Gitterschnittgra\-phen bipartit sind, haben sie eine Order Dimension. In \cite{GigDim} haben Chaplick et al. gezeigt, dass alle Gitterschnittgra\-phen eine maximale Order Dimension von 4 haben. Für \stgren{} ist diese sogar maximal 3. Im Beweis für Gitterschnittgra\-phen werden für einen Gitterschnittgra\-phen $G \in \textrm{GIG}$ vier lineare Ordnungen konstruiert, die $\Q G$ realisieren, was die obere Schranke für die Order Dimension beweist. Ist $G$ ein \stgr{}, reichen drei dieser vier linearen Erweiterungen von $\Q G$, um $\Q G$ zu realisieren. Der Beweis soll hier wiedergegeben werden.


\begin{proposition}
    \label{prop:dim_stick}
    Sei $G$ ein \stgr{}. Dann gilt $\dim (G) \leq 3$.
\end{proposition}
\begin{proof}
Sei $G \in \stick$ und $\sr{S_G}$ eine \stickrep{} von $G$. Die horizontalen Sticks von $\sr{S_G}$ seien die maximalen Elemente von $\Q G$ und die vertikalen entsprechend die minimalen. Zwei Knoten sind genau dann adjazent in $G$, wenn sich die entsprechenden Sticks in $\sr{S_G}$ schneiden und dies wiederum genau dann, wenn die entsprechenden Elemente in $\Q G$ vergleichbar sind.\\

Wir betrachten die orthogonale Projektion eines bestimmten Punktes je Stick auf drei gerichtete Geraden und konstruieren zu jeder der Geraden eine lineare Erweiterung von $\Q G$. Diese linearen Erweiterungen bezeichnen wir entsprechend der Richtung der Geraden als $\Llr$, $\Lrl$ und $\Ltb$. Die Projektionen nehmen wir wie folgt vor:
\begin{itemize}
\item[$\Lrl$:] Projektion der An\-ker\-punkte aller Sticks auf eine horizontale Gerade
\item[$\Llr$:] Projektion der An\-ker\-punkte der vertikalen Sticks und der Zielpunkte der horizontalen Sticks auf eine horizontale Gerade
\item[$\Ltb$:] Projektion der An\-ker\-punkte der horizontalen Sticks und der Zielpunkte der vertikalen Sticks auf eine vertikale Gerade
\end{itemize}
\cref{fig:dim3_konstruktion} zeigt diese Projektion beispielhaft. Bei zwei Sticks gleicher Orientierung, die denselben Stick als letztes schneiden, ver\-län\-gern wir den Stick, dessen An\-ker\-punkt weiter oben liegt, ein Stück weiter. Die Sticks $A$ und $C$ sowie $a$ und $b$ sind Beispiele dafür. So erreichen wir, dass nicht zwei Sticks auf denselben Punkt projeziert werden. Die linearen Erweiterungen erhalten wir, indem wir die projezierten Elemente in der Reihenfolge ihres Vorkommens auf der zugehörigen Geraden anordnen, wobei wir die Richtung der Geraden beachten. Aus dem Beispiel in der Abbildung ergeben sich $\Llr = [aBbCA]$, $\Lrl = [baCBA]$ sowie $\Llr = [abABC]$. 

Wir müssen nun lediglich zeigen, dass die drei linearen Erweiterungen tatsächlich $\Q G$ realisieren. Wir betrachten dazu einen beliebigen Stick $A$, der über einem beliebigen anderen Stick $a$ liegt. Es gibt drei Fälle bezüglich der Lage von $a$ und $A$ zueinander zu unterscheiden, die in den Abbildungen \ref{fig:dim3_hor_vert_nicht_schneidend} bis \ref{fig:dim3_hor_vert_schneidend} grafisch dargestellt sind:
\begin{enumerate}
    \item \textit{$A$ und $a$ haben die gleiche Orientierung}\\
    Sind beide Sticks horizontal, gilt $a \reli{\Lrl} A$ sowie $A \reli{\Ltb} a$. Bei vertikalen Sticks gilt $a \reli{\Lrl} A$ sowie $A \reli{\Llr} a$. Im Schnitt der drei linearen Erweiterungen sind $a$ und $A$ also nicht vergleichbar. Dies ist in \cref{fig:dim3_gleiche_richtung} dargestellt.
    \item \textit{$A$ und $a$ haben verschiedene Orientierungen und schneiden sich nicht}\\
    Ist $A$ vertikal und $a$ horizontal, erhalten wir $a \reli{\Lrl} A$ und $A \reli{\Llr} a$. Ist $A$ horizontal und $a$ vertikal, so endet mindestens ein Stick vor dem Punkt, an dem sie sich schneiden würden. Wir erhalten jeweils eine lineare Erweiterung, in der $a$ Vorgänger von $A$ ist und eine, in der $a$ Nachfolger von $A$ ist (siehe \cref{fig:dim3_hor_vert_nicht_schneidend}). Wieder sind $a$ und $A$ im Schnitt der drei linearen Erweiterungen nicht vergleichbar.
    \item \textit{$A$ und $a$ haben verschiedene Orientierungen und schneiden sich}\\
    Aus \cref{fig:dim3_hor_vert_schneidend} ist ersichtlich, das $a$ dann in $\Lrl$, $\Llr$ und $\Ltb$ Vorgänger von $A$ ist. Dies gilt somit auch im Schnitt der drei linearen Erweiterungen
\end{enumerate}

Insgesamt zeigt dies, dass $\mathcal L := \{\Llr, \Lrl, \Ltb\}$ das Poset $\Q G$ realisiert, da zwei Elemente im Schnitt von $\Llr$, $\Lrl$ und $\Ltb$ genau dann vergleichbar sind, wenn sich die entsprechenden Sticks schneiden.

%Die Beobachtung aus \cref{beob:realisierer} liefert uns nun, dass $\mathcal L := \{\Llr, \Lrl, \Ltb\}$ ein Realisierer von $\Q G$ ist und daraus folgt $\dim (G) \leq 3$.
\end{proof}

%Beweis Dim<=3
\begin{figure}[!htb]
\centering
\begin{subfigure}[b]{0.5\textwidth}
    \centering
    \begin{tikzpicture}[scale=0.8]
            \begin{gridComplete}{5}
                \newcommand{\PAD}{1.5}
                \newcommand{\OFF}{1}
                \stickRight 1 {5.7}
                \stickRight 2 {4.2}
                \stickRight 3 {5.2}
                \stickUp 4 {0.3}
                \stickUp 5 {0.8}
                \segmentsWider 5 {\PAD}{\OFF}
                \anchorLabel[-0.2cm] 1 {$A$}
                \anchorLabel[-0.2cm] 2 {$B$}
                \anchorLabel[-0.2cm] 3 {$C$}
                \anchorLabel[-0.2cm] 4 {$a$}
                \anchorLabel[-0.2cm] 5 {$b$}
                
                
                \node (A1) at (1,1)  {};
                \node (B1) at (2,2) {};
                \node (C1) at (3,3) {};
                \node (a1) at (4.00,4.00) {};
                \node (b1) at (5.00,5.00) {};
                \node (A2) at (6,1) {};
                \node (B2) at (4.5,2) {};
                \node (C2) at (5.5,3) {};
                \node (a2) at (4,0) {};
            \node (b2) at (5,0.5) {};
            \node (X1) at (1,1-\PAD) {};
            \node (X2) at (2,1-\PAD) {};
            \node (X3) at (3,1-\PAD) {};
            \node (X4) at (4,1-\PAD) {};
            \node (X5) at (5,1-\PAD) {};
            \node (Y1) at (5+\PAD, 0) {};
            \node (Yb) at (5+\PAD, 0.5) {};
            \node (Y2) at (5+\PAD, 1) {};
            \node (Y3) at (5+\PAD, 2) {};
            \node (Y4) at (5+\PAD, 3) {};
            \node (Z1) at (4.00, 5+\PAD) {};
            \node (Z2) at (4.5, 5+\PAD) {};
            \node (Z3) at (5.00, 5+\PAD)  {};
            \node (Z4) at (6, 5+\PAD) {};
            \node (ZC) at (5.5, 5+\PAD) {};
            
            \draw[dotted, green, very thick]
            (A1.center) -- (X1.center)
            (B1.center) -- (X2.center)
            (C1.center) -- (X3.center)
            (a2.center) -- (X4.center)
            (b2.center) -- (X5.center);
            
            \draw[dotted, blue, very thick]
            (a2.center) -- (Y1.center)
            (b2.center) -- (Yb.center)
            (A2.center) -- (Y2.center)
            (B2.center) -- (Y3.center)
            (C2.center) -- (Y4.center);

            \draw[dotted, orange, very thick]
            (a1.center) -- (Z1.center)
            (B2.center) -- (Z2.center)
            (b1.center) -- (Z3.center)
            (A2.center) -- (Z4.center)
            (C2.center) -- (ZC.center);
    
            \node[label=$A$, above=0.25 of X1]{};
            \node[label=$B$, above=0.25 of X2]{};
            \node[label=$C$, above=0.25 of X3]{};
            \node[label=$a$, above=0.25 of X4]{};
            \node[label=$b$, above=0.25 of X5]{};
            
            \node[right=0.1 of Y1]{$a$};
            \node[right=0.1 of Yb]{$b$};
            \node[right=0.1 of Y2]{$A$};
            \node[right=0.1 of Y3]{$B$};
            \node[right=0.1 of Y4]{$C$};
            
            \node[below=-0.01 of Z1]{$a$};
            \node[below=0.1 of Z2]{$B$};
            \node[below=0.1 of Z3]{$b$};
            \node[below=0.1 of Z4]{$A$};
            \node[below=0.1 of ZC]{$C$};
            
            \node[orange] at (1,6) {$\mathbf{\Llr}$};
            \node[green] at (6,-1) {$\mathbf{\Lrl}$};
            \node[blue] at (7,5) {$\mathbf{\Ltb}$};
        \end{gridComplete}
    \end{tikzpicture}
    \caption{}
    \label{fig:dim3_konstruktion}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
    \centering
    \begin{tikzpicture}[scale=0.75]

    \begin{scope}
        \begin{gridComplete}{2}
            \segmentTop 2
            %\segmentRight 2
            \segmentBottom 2

            
            \node (A1) at (1,1) {};
            \node (A2) at (1.6,1) {};
            
            \node (a1) at (2,2) {};
            \node (a2) at (2,0.6) {};
            
            \node (XA) at (1, 1-\Padding) {};
            \node (Xa) at (2,1-\Padding) {};
            \node (YA) at (2+\Padding,1) {};
            \node (Ya) at (2+\Padding,0.6) {};
            \node (Za) at (2,2+\Padding) {};
            \node (ZA) at (1.6,2+\Padding) {};
            
            \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            % \draw[dotted] (A2.center) -- (YA.center);
            % \node[right=-0.15 of YA]{$A$};
            % \draw[dotted] (a2.center) -- (Ya.center);
            % \node[right=-0.15 of Ya]{$a$};

            %Bottom
            \draw[dotted] (A2.center) -- (ZA.center);
            \node[below=0.1 of ZA]{$A$};
            \draw[dotted] (a1.center) -- (Za.center);
            \node[below=0.1 of Za]{$a$};

        \end{gridComplete}
    \end{scope}
    
    \begin{scope}[xshift=4cm]
        \begin{gridComplete}{2}
            \segmentTop 2
            %\segmentRight 2
            \segmentBottom 2

            
            \node (A1) at (1,1) {};
            \node (A2) at (1.6,1) {};
            
            \node (a1) at (2,2) {};
            \node (a2) at (2,1.4) {};
            
            \node (XA) at (1, 1-\Padding) {};
            \node (Xa) at (2,1-\Padding) {};
            \node (YA) at (2+\Padding,1) {};
            \node (Ya) at (2+\Padding,1.4) {};
            \node (Za) at (2,2+\Padding) {};
            \node (ZA) at (1.6,2+\Padding) {};
            
            \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            % \draw[dotted] (A2.center) -- (YA.center);
            % \node[right=-0.15 of YA]{$A$};
            % \draw[dotted] (a2.center) -- (Ya.center);
            % \node[right=-0.15 of Ya]{$a$};

            %Bottom
            \draw[dotted] (A2.center) -- (ZA.center);
            \node[below=0.1 of ZA]{$A$};
            \draw[dotted] (a1.center) -- (Za.center);
            \node[below=0.1 of Za]{$a$};

        \end{gridComplete}
    \end{scope}

    \begin{scope}[yshift=-4.5cm]
        \begin{gridComplete}{2}
            \segmentTop 2
            \segmentRight 2
            %\segmentBottom 2

            
            \node (A1) at (1,1) {};
            \node (A2) at (2.4,1) {};
            
            \node (a1) at (2,2) {};
            \node (a2) at (2,1.4) {};
            
            \node (XA) at (1, 1-\Padding) {};
            \node (Xa) at (2,1-\Padding) {};
            \node (YA) at (2+\Padding,1) {};
            \node (Ya) at (2+\Padding,1.4) {};
            \node (Za) at (2,2+\Padding) {};
            \node (ZA) at (2.4,2+\Padding) {};
            
            \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            \draw[dotted] (A2.center) -- (YA.center);
            \node[right=-0.15 of YA]{$A$};
            \draw[dotted] (a2.center) -- (Ya.center);
            \node[right=-0.15 of Ya]{$a$};

            %Bottom
            % \draw[dotted] (A2.center) -- (ZA.center);
            % \node[below=0.1 of ZA]{$A$};
            % \draw[dotted] (a1.center) -- (Za.center);
            % \node[below=0.1 of Za]{$a$};
        \end{gridComplete}
    \end{scope}

    \begin{scope}[xshift=4cm, yshift=-4.5cm]
        \begin{gridComplete}{2}
            \segmentTop 2
            %\segmentRight 2
            \segmentBottom 2

            
            \node (A1) at (1,1) {};
            \node (A2) at (1,0.6) {};
            
            \node (a1) at (2,2) {};
            \node (a2) at (2.4,2) {};
            
            \node (XA) at (1, 1-\Padding) {};
            \node (Xa) at (2.4,1-\Padding) {};
            \node (YA) at (2+\Padding,0.6) {};
            \node (Ya) at (2+\Padding,2) {};
            \node (Za) at (2,2+\Padding) {};
            \node (ZA) at (1,2+\Padding) {};
            
            \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            % \draw[dotted] (A2.center) -- (YA.center);
            % \node[right=-0.15 of YA]{$A$};
            % \draw[dotted] (a2.center) -- (Ya.center);
            % \node[right=-0.15 of Ya]{$a$};

            %Bottom
            \draw[dotted] (A2.center) -- (ZA.center);
            \node[below=0.1 of ZA]{$A$};
            \draw[dotted] (a1.center) -- (Za.center);
            \node[below=0.1 of Za]{$a$};

        \end{gridComplete}
    \end{scope}

    \end{tikzpicture}
\caption{}
\label{fig:dim3_hor_vert_nicht_schneidend}
\end{subfigure}
\begin{subfigure}[b]{0.66\textwidth}
    \centering
    \begin{tikzpicture}[scale=0.75]
    \begin{scope}
        \begin{gridComplete}{2}
            \segmentTop 2
            \segmentRight 2
            %\segmentBottom 2

            
            \node (A1) at (1,1) {};
            \node (A2) at (1.4,1) {};
            
            \node (a1) at (2,2) {};
            \node (a2) at (2.4,2) {};
            
            \node (XA) at (1, 1-\Padding) {};
            \node (Xa) at (2.4,1-\Padding) {};
            \node (YA) at (2+\Padding,1) {};
            \node (Ya) at (2+\Padding,2) {};
            \node (Za) at (2,2+\Padding) {};
            \node (ZA) at (2.4,2+\Padding) {};
            
            \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            \draw[dotted] (A2.center) -- (YA.center);
            \node[right=-0.15 of YA]{$A$};
            \draw[dotted] (a2.center) -- (Ya.center);
            \node[right=-0.15 of Ya]{$a$};

            %Bottom
            % \draw[dotted] (A2.center) -- (ZA.center);
            % \node[below=0.1 of ZA]{$A$};
            % \draw[dotted] (a1.center) -- (Za.center);
            % \node[below=0.1 of Za]{$a$};
        \end{gridComplete}
    \end{scope}

    \begin{scope}[xshift=4cm]
        \begin{gridComplete}{2}
            \segmentTop 2
            %\segmentRight 2
            \segmentBottom 2

            
            \node (A1) at (1,1) {};
            \node (A2) at (1,0.6) {};
            
            \node (a1) at (2,2) {};
            \node (a2) at (2,1.6) {};
            
            \node (XA) at (1, 1-\Padding) {};
            \node (Xa) at (2,1-\Padding) {};
            \node (YA) at (2+\Padding,0.6) {};
            \node (Ya) at (2+\Padding,2) {};
            \node (Za) at (2,2+\Padding) {};
            \node (ZA) at (1,2+\Padding) {};
            
            \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            % \draw[dotted] (A2.center) -- (YA.center);
            % \node[right=-0.15 of YA]{$A$};
            % \draw[dotted] (a2.center) -- (Ya.center);
            % \node[right=-0.15 of Ya]{$a$};

            %Bottom
            \draw[dotted] (A2.center) -- (ZA.center);
            \node[below=0.1 of ZA]{$A$};
            \draw[dotted] (a1.center) -- (Za.center);
            \node[below=0.1 of Za]{$a$};

        \end{gridComplete}
    \end{scope}

    \end{tikzpicture}
\caption{}
\label{fig:dim3_gleiche_richtung}
\end{subfigure}
\begin{subfigure}[b]{0.33\textwidth}
    \centering
    \begin{tikzpicture}[scale=0.75]
        \begin{scope}
            \begin{gridComplete}{2}
                \segmentTop 2
                \segmentRight 2
                \segmentBottom 2
                
                
                \node (A1) at (1,1) {};
                \node (A2) at (2.4,1) {};
                
                \node (a1) at (2,2) {};
                \node (a2) at (2,0.6) {};
                
                \node (XA) at (1, 1-\Padding) {};
                \node (Xa) at (2,1-\Padding) {};
                \node (YA) at (2+\Padding,1) {};
                \node (Ya) at (2+\Padding,0.6) {};
                \node (Za) at (2,2+\Padding) {};
                \node (ZA) at (2.4,2+\Padding) {};
                
                \draw[very thick] (A1.center) -- (A2.center);
            \draw[very thick] (a1.center) -- (a2.center);
            
            %\stickRight 1 {2}
            %\stickUp 2 {1}
            
            %Top
            \draw[dotted] (A1.center) -- (XA.center);
            \node[label=$A$, above=0.4 of XA]{};
            \draw[dotted] (a2.center) -- (Xa.center);
            \node[label=$a$, above=0.4 of Xa]{};

            %Right
            \draw[dotted] (A2.center) -- (YA.center);
            \node[right=-0.15 of YA]{$A$};
            \draw[dotted] (a2.center) -- (Ya.center);
            \node[right=-0.15 of Ya]{$a$};
            
            %Bottom
            \draw[dotted] (A2.center) -- (ZA.center);
            \node[below=0.1 of ZA]{$A$};
            \draw[dotted] (a1.center) -- (Za.center);
            \node[below=0.1 of Za]{$a$};
            
        \end{gridComplete}
    \end{scope}
\end{tikzpicture}
    \caption{}
    \label{fig:dim3_hor_vert_schneidend}
\end{subfigure}
\caption{Zum Beweis, dass $\dim (G) \leq 3$ für einen \stgren{} $G$ gilt.}
\label{fig:dim_stick}
\end{figure}