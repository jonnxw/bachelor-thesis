\FloatBarrier 
\subsection{\entwurf{}Posets}
\begin{definition*}
    Eine \begriffkurz{halbgeordnete Menge}{\textbf partial \textbf ordered \textbf{set}}{Poset} $\mathcal{P} = (P, \rel)$ besteht aus einer Menge $P$ und einer binären, irreflexiven, transitiven Relation $\rel$ auf $P$. 
    \label{def:poset}
\end{definition*}
Als \textit{binäre Relation auf $P$} ist $\rel$ eine Teilmenge des kartesischen Produktes $P \times P$. Wird sie nicht explizit benannt, kann mit $\reli P$ auf die zu $\mathcal P$ gehörende Relation verwiesen werden. Wenn $(a,b) \in \rel$ gilt, so sind $a$ und $b$ \textit{vergleichbar}. Man schreibt üblicherweise $a \rel b$, $a$ ist dann \textit{Vorgänger} von $b$ und $b$ ist \textit{Nachfolger} von $a$. Für $(a,b) \notin \rel$ schreibt man $a \nrel b$, $a$ und $b$ sind dann \textit{nicht vergleichbar}. \textit{Irreflexivität} bedeutet, dass $a \nrel a$ für alle $a\in P$ gilt und die Forderung der \textit{Transitivität} besagt, dass aus $a \rel b$ und $b \rel c$ auch $a \rel c$ folgt. Diese Definition eines Posets impliziert, dass $\rel$ auch \textit{antisymmetrisch} ist. Für $a \neq b$ gilt also nicht gleichzeitig $a \rel b$ und $b \rel a$: Angenommen, es gilt $a \rel b$ und $b \rel a$. Dann folgt aus der Transitivität, dass $a \rel a$ gilt, was ein Widerspruch zur Irreflexivität ist. Gibt es für ein Element $a \in P$ \textit{kein} Element $b \in P$ mit $b \rel a$ (bzw. $a \rel b$), so heißt $a$ \textit{minimales} (bzw. \textit{maximales}) Element von $\mathcal{P}$. 

In einigen Quellen wird die Irreflexivität durch Reflexivität und Antisymmetrie ersetzt. Die hier gegebene Definition deckt sich mit den Quellen \cite{Posets} und \cite{TrotterDimensionComparability}. Sie entspricht im Deutschem einer \textit{strengen Halbordnung}.\\

\begin{wrapfigure}{l}{8em}
    \tikz{
\node (X) at (0,2) {$\{1,2\}$};
\node (1) at (-1,1) {$\{1\}$};
\node (2) at (1,1) {$\{2\}$};
\node (empty) at (0,0) {$\emptyset$};
\draw (X) -- (1) -- (empty) -- (2) -- (X) -- (empty);
}
\end{wrapfigure}
\noindent Ein typisches Beispiel für ein Poset ist die Potenzmenge $2^X$ einer endlichen Menge $X$ mit der \textit{(echten) Inklusionsrelation}: $A \subsetneq A$ kann per Definition nicht gelten, für Mengen $A,B,C \subseteq X$ gilt nicht gleichzeitig $A \subsetneq B$ und $B \subsetneq A$ und aus $A \subsetneq B$ und $B \subsetneq C$ folgt $A \subsetneq C$. In diesem Poset ist $\emptyset$ ein minimales und $X$ ein maximales Element. Die nebenstehende Zeichnung illustriert dieses Poset für $X=\{1,2\}$, wobei \enquote{\textit{kleinere}} Elemente unter \enquote{\textit{größeren}} gezeichnet sind mit einer Kante zwischen zwei Elementen, wenn diese vergleichbar sind.

% \begin{definition*}
%     Ein Poset $\mathcal{L} = (L, \rel_L)$ heißt \textit{lineare Ordnung}, wenn je zwei verschiedene Elemente vergleichbar sind. $\mathcal{L} = (L, \rel_L)$ ist eine \textit{lineare Erweiterung} des Posets $\mathcal{P} = (P, \rel_P)$, falls $L = P$ und $\rel_P~\subseteq~\rel_L$ gilt.
%     \label{lineare_Erweiterung}
% \end{definition*}
\begin{definition*}
    Sei $\mathcal{L} = (L, \rel_L)$ ein Poset.
    \begin{itemize}
        \item $\mathcal{L}$ heißt \textit{lineare Ordnung}, wenn je zwei verschiedene Elemente vergleichbar sind.
        \item $\mathcal{L}$ ist eine \textit{lineare Erweiterung} des Posets $\mathcal{P} = (P, \rel_P)$, wenn $L = P$ und $\rel_P~\subseteq~\rel_L$ gilt.
    \end{itemize} 
    \label{lineare_Erweiterung}
\end{definition*}
Für die Elemente $l_i \in L$ ergibt sich eine Reihenfolge $l_1 \rel_L l_2 \rel_L \ldots \rel_L l_n$ und man kann $\mathcal L=[l_1l_2\ldots l_n]$ schreiben. $\mathcal{L}$ ist eine lineare Erweiterung von $\mathcal P$, wenn $\mathcal{L}$ die Vergleichbarkeiten von $\mathcal P$ \enquote{\textit{erhält}}, das heißt, wenn aus $a \rel_P b$ folgt, dass auch $a \rel_L b$ gilt.\\

Sind $\mathcal{P}_1 = (P, \rel_1)$ und $\mathcal{P}_2 = (P, \rel_2)$ zwei Posets über der gleichen Menge $P$, so heißt $$\mathcal P_1 \cdot \mathcal P_2 := (P,~\rel_1\cap\rel_2)$$ der \textit{Schnitt von} $\mathcal P_1$ und $\mathcal P_2$. Ein Element $a\in P$ ist genau dann Vorgänger von $b\in P$ in $\mathcal P_1 \cdot \mathcal P_2$, wenn $a$ Vorgänger von $b$ in $\mathcal P_1$ und in $\mathcal P_2$ ist. Der Schnitt zweier Posets ist stets ein Poset. Analog definiert man $\prod \mathcal{P}_i$ als den Schnitt von endlich vielen Posets $\mathcal{P}_1, \ldots, \mathcal{P}_k$. Man sagt, eine Familie $\mathcal{R} = \{\mathcal L_1, \cdots, \mathcal L_k\}$ linearer Erweiterungen \textit{realisiert} $\mathcal{P}$, falls $$\mathcal P = \prod_{L \in \mathcal R} L$$ gilt. $\mathcal R$ ist dann ein \textit{Realisierer} von $\mathcal P$. \\

B. Dushnik und E. Miller definieren in ihrer Arbeit \cite{Posets} erstmals die \textit{Dimension} eines Posets wie folgt:
\begin{definition*}
    Die \textit{Dimension} $\dim(\mathcal P)$ eines Posets $\mathcal{P}$ ist die kleinste Zahl $k$, sodass eine Familie von linearen Ordnungen $\mathcal{L} = \{\mathcal L_1, \cdots, \mathcal L_k\}$ existiert, die $\mathcal{P}$ realisiert. $\mathcal L$ wird \textit{minimaler Realisierer} von $\mathcal P$ genannt.
    \label{def:dim_poset}
\end{definition*}
In ihrer Arbeit beweisen die beiden Autoren, dass es zu jedem Poset $\mathcal P$ stets eine Familie von linearen Ordnungen gibt, die $\mathcal P$ realisiert. Damit ist die Wohldefiniertheit der Dimension eines Posets gewährleistet. In Abgrenzung zu anderen Dimensionsbegriffen wird die Dimension eines Posets auch als \textit{Order Dimension} bezeichnet. 

% Aus der Definition eines Realisierers $\mathcal L$ eines Posets $\mathcal P$ folgt direkt ein Zusammenhang zwischen den Vergleichbarkeiten von $\mathcal P$ und den linearen Ordnungen $\mathcal L_i \in \mathcal L$:
% \begin{beobachtung}
%     \label{beob:realisierer}
%     Zwei Elemente $a,b \in \mathcal P$ sind genau dann nicht vergleichbar, wenn es zwei lineare Erweiterungen $\mathcal L_1, \mathcal L_2 \in \mathcal L$ gibt mit $a \rel_{\mathcal L_1}  b$ und $b \rel_{\mathcal L_2} a$.
% \end{beobachtung}