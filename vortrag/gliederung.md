# Vorstellung
- Name, BA geschrieben, möchte gerne vorstellen
- Grundbegriffe zu Graphen nicht extra definiert, Notationen werden On-The-Fly eingeführt --> Falls Notation unklar / nicht erklärt, bitte nachfragen

# Einführung
- Was sind Stick-Graphen?
  - geom. Schnittgraphen -> Gitterschnittgraphen -> Stick-Graphen
- Recognition Problem / Identifizierungsproblem: Offen, ob in polynomieller Zeit möglich
- Ziel der Arbeit: Strukturen untersuchen, die dazu führen, dass Graph kein Stick-Graph ist
  - -> minimale Nicht-Stick-Graphen. Was sind Nicht-Stick-Gr. und wann ist solcher minimal?
- Stick-Graphen sind bipartit. Wenn Graph nicht bipartit, dann kein Stick-Graph --> Nicht bipartite sind uninteressant -> Nicht-Stick-Graphen sind BIPARTITE Graphen, die keine Stick-Graphen sind
- Def. minimaler Nicht-Stick-Graph 

## Warum sind die minimalen Nicht-Stick-Graphen interessant / relevant?
- Lemma: G Stick -> G[W] Stick
- (Gilt allgemein für Schnittgraphen)
- Kontraposition: Wenn induzierter TG Nicht-Stick-Gr, dann ist Graph auch Nicht-Stick-Gr
- Wenn Gr. Nicht-Stick, dann enthält er minimalen Nicht-Stick als induzierten
- --> minimale NS können dazu benutzt werden um zu erkennen, ob ein Graph NS ist oder Stick-Graph
- Ziel / Ergebnisse der BA
  - 9 minimale Nicht-Stick mit maximal 10 Knoten
  - unendliche Familie von Nicht-Stick (Rampe)
  - unendliche Familie von Nicht-Stick (Möbius)


# Begriffe & Vorbetrachtungen
- maximal reduziert
  - kongruente Knoten
  - kein Grad-1-Knoten
  - Zusammenhängend
- Lemma: G Stick <-> G_R Stick
- Lemma: G minimal Nicht-Stick -> G maximal reduziert
- -> Interesse nur für maximal reduzierte
- Iso/Auto???
- Knotentransitivität + Wie das in Beweisen hilfreich ist

# Algorithmus & Daten
- Ansatz: Erzeugen aller Stickrep. --> Erzeugen aller max. red. Stickgr (mit n Knoten)
- Was heißt alle? Gibt es endlich viele?
- --> kanonische Stickrep / Stickvektor

## Implementierung
- Ansatz über Färbungen
- SPIEGELUNG --> Anzahl Färbungen reduzieren
- Was macht man mit einzelner Färbung? Erst horizontale, dann vertikale bestimmen
- Ergebnisse in Zahlen
- Erzeugen der biparitten und NS (jeweils max. red.)
- Finden der minimalen NS

# Ergebnisse
|  n        | 8 | 9 | 10 | 11  | 12   |
|-----------|---|---|----|-----|------|
| #NS_R(n) | 1 | 2 | 29 | 274 | 5544 |
| #min NS  | 1 | 0 | 8  | 24  | 218  |

- Gibt nur 3 bipartite, max. red. Graphen, diese alle Stick
- --> Der gefundene mit 8 Knoten ist minimal. WÜRFELGRAPH. Die beiden mit 9 Knoten enthalten Würfel als induzierten
- Warum ist Würfel NS? --> ORDER DIMENSION!!!
- Kurze Einführung zu Order Dim.
- Ergebnis Chaplick et. al: Stick-Graphen haben Order Dim. maximal 3
- Zeigen: Würfel hat Dim. 4 --> KRONE bzw. Rampe(C_6)
- Zeige hier kurz Weg über Rampe, Krone kommt später

# Rampe

# min NS mit 10 Knoten
- 8 Stück mit 10 Knoten
  - Rampe (C_8)
  - 1x fällt aus der Reihe (3 mögliche Stickrep. in unduziertem)
  - 5x mit eindeutiger Stickrep als induziertem
  - Möbiusleiter

- Den der aus Reihe fällt lass ich weg, Beweis ist Fallunterscheidung
- Rampe kennen wir schon -> Dimensionsargument
  
# Eindeutige Stickrep
- Kreis
- Mundgraph

# Krone
- Def. Krone. 
- Würfel + Möbius mit 5 Sprossen ist Krone mit Dim. 4
- großes Finale: MÖBIUSLEITE IST UNENDLICHE FAMILIE VON MINIMALEN NS