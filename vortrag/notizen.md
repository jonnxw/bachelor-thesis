# Einführung
- Name, BA geschrieben, möchte gerne vorstellen
- Grundbegriffe zu Graphen nicht extra definiert, Notationen werden On-The-Fly eingeführt --> Falls Notation unklar / nicht erklärt, bitte nachfragen

## Was sind Stick-Graphen?

## Methode

## Überblick Ergebnisse

## Voraussetzungen an Graphen
### Maximal reduzierte
  
# Algorithmus + Implementierung
### Methode
### Ansatz
### Der Algorithmus
### Ergebnisse in Zahlen
- Zahlen zu n=1,...,7 erklären
  - n=1,...,5: Partitionsklassen sind 2-3 --> kongruente Knoten
  - n=6,7: Mit Überlegungen über größen der Partitionsklassen konkret bestimmbar

## Finden der minimalen Nicht-Stick-Graphen

## Implementierung
### Stickrep. als Vektoren
### Spiegelungen

# Ergebnisse & Theorie
- Alle vorstellen, zwei Methoden nennen
  - Eindeutige Stickrepräsentationen
  - Order Dimension

## Eindeutige Stickrepräsentation
- Knotentransitivität: anschaulich mit C_4 -> Umbenennung der Knoten, so dass Strukur gleich bleibt -> Übertragung auf Stickrepr.

## Posets
- Definition + Definition Dimension ganz kurz
### Von Posets zu bipartiten Graphen
- Ergebnis aus GRID INTERSECTION GRAPHS AND ORDER DIMENSION
### Rampe (unendliche Fam. von Nicht-Stick-Graphem)
- Würfel und Rampe C_8: alternative Zeichnung
- Kreis (blau)
  - Hat Dimension 3 (Permutationsgraphen)
  - nicht-inklusiv beschreiben: Zwei Knoten: Gibt zwei weitere, die zu je einem adjazent sind und zu anderem nicht.
- Beweisansatz: kritische Paare
  - Realisierer von Graph ist auch Realisierer von induziertem Teilgraph (Knotenmenge eingeschränkt)
  - minimaler Realisierer Realisierer von Rampe hat lin. Erw, die in minimalem Realisierer von Original nicht gebraucht wird 

### Krone
- Nicht jede Möbiusleiter hat dim 4
- In Arbeit von Trotter sogar: Möbiusleiter mit mehr als 10 Knoten hat dim 3
pdfpc -g presentation.pdf 
