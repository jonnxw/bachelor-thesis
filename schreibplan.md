
## Nachgang
- [ ] TeX Dateien aufräumen
- [ ] Git Rebase
  




# Besprechung 27.11
- Einführung: Dimension-4-Argument erklären (Warum haben alle Stick dim<=3?)
- Beide andere Arbeiten zumindest nennen
- Beweis Kreis eindeutig: Mit Pfad argumentieren. Fange mit einem Knoten an und schaue, wo benachbarte Knoten gleicher Klasse in Stickrep. sein können. Blockierende Sticks. Iterativ argumentieren.
- Für Würfel: Alle stickrep. für induzierte auflisten?
- Für minimale mit 10 Knoten: Sagen, dass offensichtlich keiner den Würfel enthält --> Minimalität
## minimale NS
### Nr.1 
- Ist Möbiusleiter. Diese wiederum ist Generalized Crown (Trotter, 1973) mit n=4, k=1. Laut Formel ist dim=4. Würfel ist generalized Crown mit n=3,k=1, auch dim= 4 --> Beweise für Würfel und Möbius
- Frage: Sind die Möbiusleitern alle NichtStick? Haben wir eine Familie gefunden? Sind sie alle minimal? Für n= 12 in Daten schauen
### Nr.8
- Beweis für Dimension = 4: Siehe Zettel. Struktur des Posets: Kreis hat dim3. Brauche 4. erweriterung
- Dafür: ZEigen oder suchen: Kreis hat dim 3.
- Dann: verallgemeinern: Klasse von NS Graphen gefunden. 
- Aber: Nicht minimal, da Graph BLA (siehe Zettel) nicht stick? Beweisen. Alle enthalten ihn als induzierten TG
### Graph BLA
- in Daten schauen: ist minimal? 11 Knoten
- Beweis führen und mit aufnehmen. Wie zeigen, dass alle induzierten keinen anderen minialen enzhalten
## Zusammenfasung
- BEweis Kreis einfacher
- Dimension von Würfel und Möbius5 über generalized Crown. Zu untersuchen: Möbiusleiter als Fam. von NS? 
- Graph Nr. 8 als Familie von NS, aber nicht minimal 
- Graph BLA mit aufnehmen als einzigen mit 11 Knoten
  
## EINDEUTIGE REPRÄSENTATIONEN
- Beweis Eindeutigkeit Kreis ausarbeiten
- Beweis Kreis aufschreiben
- Beweis Eindeutigkeit Mund ausarbeiten
- Graph Mund und k-Mund definieren
- Beweis Mund aufschreiben
- Korollar k-Mund
- Symmetrie ausarbeiten. Wie klug aufschreiben? "Kanonische Rep." mit mehr horizontalen als vertikalen?
- Symmetrie aufschreiben
- Def. k-Mund mit zusätzlichen (vollständigen Ecken)
- Korollar dazu. Symmetrie!!!
- k-Mund mit verbundenen Zusatzecken AUSARBEITEN
  - Welche Konstruktionen sind eindeutig?
  - Welche Nicht?
  - Gibt es welche, die gar nicht stick sind?
    
## Erweitern von Stickrepresäntationen
- Fragestellung formulieren
- Strukturieren (versuchen:)
- Separierende Hacken
- Blockierende Sticks

## Forschungsstand / Einführung

## Stick bis 12 Knoten
- Struktur des Kapitels verfeinern
  
# 18. - 22.12
- Definitionen, Lemma, Sätze, Propositionen, Korollare schonmal aufschreiben?



# FALLS ZU VIEL ZEIT
- Stickgraphen mit vielen Repräsentationen? 
- Code minimal NS für n=11: Haben 10-Kreis, 4-Mund, 8-Kreis oder 3-Mund?
- Eindeutigkeit von Stickrepr.: Auf Symmetrie eingehen. Relevant wenn Partitionsklassen nicht gleich groß
- farben red green blue schöner wählen

# Hinweise
## Einführung
## Eindeutige Repräsentationen
## Erweitern
## Stick bis 12
## Minimale Stick
